﻿using Stepper.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stepper.ViewModels
{
    class MainViewModel: INotifyPropertyChanged
    {
        private int val;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Val
        {
            get { return val; }
            set 
            { 
                val = value;
                // signaler à la vue du changement de la propriété
                OnPropertyChanged();
            }
        }

        public RelayCommand MyCommand { get; set; }
        public RelayCommand MyCommand2 { get; set; }

        public MainViewModel()
        {
            MyCommand = new RelayCommand(Augmenter);
            MyCommand2 = new RelayCommand(Diminuer);
        }

        public void Augmenter()
        {
            Val++;
        }

        public void Diminuer()
        {
            Val--;
        }

        private void OnPropertyChanged([CallerMemberName]string propName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

    }
}
